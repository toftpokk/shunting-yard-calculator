
# -*- coding: utf-8 -*-
"""
Author: toftpokk
Created: 09/09/20
"""

def tokenize(raw):
    """
    Tokenize the input string and outputs an list of integers and operator chars

    Arguments:
        raw -- Raw input string without errors
    
    Returns:
        List -- Resulting token list
    """
    i = 0
    res = []
    while i <len(inp):
        if inp[i] in [ str(x) for x in range(0,10)]:
            tmp = ""
            while i<len(inp) and inp[i] in [str(x) for x in range(0,10)]:
                tmp+=inp[i]
                i+=1
            res.append(int(tmp))
        else:
            res.append(inp[i])
            i+=1
    return res

def shunting_yard(inp):
    """
    The Shunting-Yard algorithm which only checks + - * /
    From https://en.wikipedia.org/wiki/Shunting-yard_algorithm

    Arguments:
        inp -- Input token list
    
    Returns:
        List -- Resulting Postfix list
    """
    precedence = {
        "+": 1,
        "-" : 1,
        "*": 2,
        "/": 2
    }
    op_stack = []
    res_stack = []
    i = 0
    while i < len(inp):
        token = inp[i]
        if type(token) == int:
            res_stack.append(token)
        elif token in precedence.keys():
            while op_stack != [] and op_stack[-1] != "(" and (precedence[token] <= precedence[op_stack[-1]]):
                res_stack.append(op_stack.pop())
            op_stack.append(token)
        elif token == "(":
            op_stack.append(token)
        elif token == ")":
            while op_stack[-1] != "(":
                res_stack.append(op_stack.pop())
            if op_stack[-1] == "(":
                op_stack.pop()
        i+=1
    while op_stack != []:
        res_stack.append(op_stack.pop())
    return res_stack

def ev(postfix):
    """
    Postfix Evaluator for Shunting-Yard Algorithm
    https://www.youtube.com/watch?v=n8s7OKSxRxU

    Arguments:
        postfix -- Postfix list of integers and operators

    Returns:
        Int -- Result of Calculaton
    """
    res_stack = []
    for i in postfix:
        if type(i) == int:
            res_stack.append(i)
        else:
            a = res_stack.pop()
            b = res_stack.pop()
            if i == "+":
                res = b+a
            elif i == "-":
                res = b-a
            elif i == "*":
                res = b*a
            elif i == "/":
                res = b/a
            res_stack.append(res)
    return res_stack[0]
    
if __name__ == "__main__":
    inp = " 3+4*2/(1-5)"
    tokens = tokenize(inp)
    res_stack = shunting_yard(tokens)
    print(ev(res_stack))
